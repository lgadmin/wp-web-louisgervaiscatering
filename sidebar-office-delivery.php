<?php
/**
 * The sidebar containing the main widget area
 *
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {return; } ?>

<aside id="secondary" class="widget-area">
	
	<?php dynamic_sidebar( 'office-page-1' ); ?>

	<section class="cta-testimonials-slider mt-lg">
		<h2 class="h3">Kind Words</h2>
		<?php get_template_part( 'templates/template-parts/cta-testimonials-slider'); ?>
		<a href="/about/testimonials/" class="btn btn-primary mt-sm">More Kind Words</a>
	</section>
	
</aside>
