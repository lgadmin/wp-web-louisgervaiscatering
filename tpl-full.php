<?php
/**
 * Template Name: Full width
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container-fluid">
				<div class="body-copy">
					<?php get_template_part( 'templates/template-parts/content' ) ?>
				</div>
			</main>
		</div>
	</div>

<?php get_footer();
