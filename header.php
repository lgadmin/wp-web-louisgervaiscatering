<?php
/**
 * The header for our theme
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class('bg-image-alpha'); ?>>

<div class="hidden">

</div>

  <header id="masthead" class="site-header bg-image-chalk">
		
		<div class="site-branding site-header-alpha">	
      	<?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?>
		</div>

    <div class="tagline"></div>
    
    <div class="main-navigation site-header-bravo">

        <div class="header-utilit-cont">
         
          <section class="header-utility text-right mt-md mb-sm">
            <ul class="list-inline">
              <li><a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><span class="sr-only">Phone</span><i class="fa fa-phone" aria-hidden="true"></i> <?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></li>
              <li><a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><span class="sr-only">Phone</span><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo do_shortcode('[lg-email]'); ?></a></li>
              <li>
                <a href="https://www.kixpayments.com/louisgervais">
                  <span class="sr-only">Phone</span><i class="fa fa-usd" aria-hidden="true"></i> Pay Online!</a>
              </li>
            </ul>
          </section>

          <section class="text-right">
            <?php echo do_shortcode( '[lg-social-media]' ); ?>
          </section>
        </div>

      	<nav class="navbar navbar-default">  
      	    <!-- Brand and toggle get grouped for better mobile display -->
      	    <div class="navbar-header">

      	      <div class="toggle-cont">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
        	        <span class="sr-only">Toggle navigation</span>
        	        <span class="icon-bar"></span>
        	        <span class="icon-bar"></span>
        	        <span class="icon-bar"></span>
        	      </button>
              </div>
      	    
              <div class="navbar-header-logo"> <?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?> </div>

            </div>


            <!-- Main Menu  -->
            <?php 

              $mainMenu = array(
               	'depth'             => 2,
              	'container'         => 'div',
              	'container_class'   => 'collapse navbar-collapse',
              	'container_id'      => 'main-navbar',
              	'menu_class'        => 'nav nav-justified navbar-nav',
              	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
              	// 'walker'         => new WP_Bootstrap_Navwalker_Custom()  // Custom used in Skin Method.
              	'walker'            => new WP_Bootstrap_Navwalker()
              );

              wp_nav_menu($mainMenu);

            ?>
      	</nav>
    </div>
  </header>
