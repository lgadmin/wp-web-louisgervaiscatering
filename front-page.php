<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			

			<main class="container clearfix bg-white">
				
				<section class="home-slider mt-lg">
					<?php get_template_part( 'templates/template-parts/feature-slider-front' ); ?>
				</section>

				<section class="image-bar-container">
					<?php get_template_part( 'templates/template-parts/image-bar' ); ?>
				</section>

				<section class="cta-nav">
					<?php get_template_part( 'templates/template-parts/cta-quick-nav-front' ); ?>
				</section>
				
				<div class="body-copy">
					<?php get_template_part( 'templates/template-parts/content' ); ?>
				</div>

				<section class="fp-blog-feed mb-lg pr-lg"> <?php get_template_part( 'templates/template-parts/blog-feed-front', 'front' ); ?> </section>

				<section class="fp-form col-5 mb-lg">
					<h2 class="h3">Request a Catering Quote Now</h2>
					<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
				</section>

				<section class="cta-testimonials-slider mb-sm">
					<h2 class="h3">Kind Words</h2>
					<?php get_template_part( 'templates/template-parts/cta-testimonials-slider'); ?>
					
				</section>

			</main>
		</div>
	</div>

<?php get_footer(); ?>