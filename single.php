<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container clearfix bg-white">
				
				<h2 class="page-title mt-md"><?php single_post_title(); ?></h2>
				
				<?php get_template_part( 'templates/template-parts/breadcrumb' ) ?>
				
				<div class="page-heading">
					<?php if (get_field('page_heading_h1')) : ?><h1><?php the_field('page_heading_h1'); ?></h1><?php endif; ?>
					<?php if (get_field('page_heading_h1')) : ?><p><?php the_field('page_heading_h1_small'); ?></p><?php endif; ?>
				</div>

				<div class="body-copy">
					<?php get_template_part( 'templates/template-parts/content', 'single' ); ?>

					<div class="blog-nav mb-lg">
						
						<?php if(get_next_post()): ?>
							<a href="<?php echo get_permalink( get_next_post()->ID ); ?>" class="previous cta btn btn-default mr-xs"><i class="fa fa-angle-left" aria-hidden="true"></i> <?php echo get_next_post()->post_title; ?></a>
						<?php endif; ?>

						<?php if(get_previous_post()): ?>
							<a href="<?php echo get_permalink( get_previous_post()->ID ); ?>" class="next cta btn btn-default"><?php echo get_previous_post()->post_title; ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
						<?php endif; ?>

					</div>

					<section class="mb-lg">
						<?php get_template_part( 'templates/template-parts/blog-about-this-author' ); ?>
					</section>

				</div>

				<?php get_sidebar(); ?>
			</main>
		</div>
	</div>

<?php get_footer(); ?> 