<?php
/**
 * The sidebar containing the main widget area
 *
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {return; } ?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'page-side-1' ); ?>
</aside>
