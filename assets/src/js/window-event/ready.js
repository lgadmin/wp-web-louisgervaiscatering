// Windows Ready Handler

(function($) {

    $(document).ready(function(){
        
        // Default Page Slider
        jQuery('.feature-slider').slick({
			lazyLoad       : 'ondemand',
			infinite       : true,
			autoplay       : true,
			slidesToShow   : 1,
			slidesToScroll : 1,
			speed          : 1000,
			fade           : true,
			cssEase        : 'linear',
			arrows         : false,
			dots           : false
        });

		// Home Page Slider
	    jQuery('.home-slider').slick({
			lazyLoad       : 'ondemand',
			infinite       : true,
			autoplay       : true,
			slidesToShow   : 1,
			slidesToScroll : 1,
			speed          : 1000,
			fade           : true,
			cssEase        : 'linear',
			arrows         : false,
			dots           : false
        });

		// Testimonials Slider
	    jQuery('.testimonial-slider').slick({
			// lazyLoad       : 'ondemand',
			infinite       : true,
			// autoplay       : false,
			slidesToShow   : 1,
			slidesToScroll : 1,
			// speed          : 1000,
			// fade           : true,
			// cssEase        : 'linear',
			arrows         : true,
			// dots           : false,
			adaptiveHeight : true,
        });

		// Awards Slider
	    jQuery('.awards-slider').slick({
			lazyLoad       : 'ondemand',
			infinite       : true,
			autoplay       : true,
			slidesToShow   : 1,
			slidesToScroll : 1,
			// centerMode     : true,
			arrows         : true,
        });

        // Mobile Image Slider
        jQuery('.image_bar').slick({
        	lazyLoad		: 'ondemand',
        	infinite 		: true,
        	autoplay 		: true,
        	fade 			: false,
        	arrows 			: false,
        	slidesToShow: 7,
		    slidesToScroll: 1,
        	responsive: [
		    {
		      breakpoint: 1441,
		      settings: {
		        slidesToShow: 6,
		        slidesToScroll: 1,
		        infinite: true,
		      }		    
		    },
		    {
		      breakpoint: 1025,
		      settings: {
		        slidesToShow: 5,
		        slidesToScroll: 1,
		      }
		    },
		    {
		      breakpoint: 769,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 426,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1
		      }
		    }
		    ]
        });

        console.log('test');

        $('.popup-form .cta').on('click', function(){
        	$(this).closest('.popup-form').toggleClass('active');
        });

    });

}(jQuery));
