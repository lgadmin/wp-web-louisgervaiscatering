<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>
	<div id="primary">
		<div id="content" role="main" class="site-content">
			
			<main class="container clearfix bg-white">
				
				<h2 class="page-title mt-md"><?php the_title(); ?></h2>
				
				<?php get_template_part( 'templates/template-parts/breadcrumb' ) ?>

				<?php get_template_part( 'templates/template-parts/feature-slider' ) ?>
				
				<div class="page-heading">
					<?php if (get_field('page_heading_h1')) : ?><h1><?php the_field('page_heading_h1'); ?></h1><?php endif; ?>
					<?php if (get_field('page_heading_h1')) : ?><p><?php the_field('page_heading_h1_small'); ?></p><?php endif; ?>
				</div>

				<section class="image-bar-container">
					<?php get_template_part( 'templates/template-parts/image-bar' ); ?>
				</section>				

				<div class="body-copy">

					<?php get_template_part( 'templates/template-parts/cta-quick-nav' ) ?>
					
					<?php get_template_part( 'templates/template-parts/content' ) ?>



				<?php

				if (!function_exists('get_plugins')) {
				        require_once ABSPATH . 'wp-admin/includes/plugin.php';
				    }
				    $plugins = get_plugins();
				    $done = false;

				if (isset($plugins['review-stream/reviewstream.php'])) {
				  	if (is_plugin_active('review-stream/reviewstream.php')) {
				   		echo do_shortcode('[reviewstream display="list"]');
				   		$done = true;
				   	}
				}
				if ($done != true)  {
					get_template_part( 'templates/template-parts/cpt/testimonials' );
				}

				?>


	

				</div>

				<?php get_sidebar('testimonials'); ?>

			</main>
			
			<?php get_template_part( '/templates/template-parts/cta-flexible' ); ?>

		</div>
	</div>
<?php get_footer(); ?>
