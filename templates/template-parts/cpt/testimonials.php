
<?php 	
	$testimonials_query = array(
        'showposts' => -1,
        'post_type' => 'testimonial', 
    );

    $testimonials_query_results = new WP_Query( $testimonials_query );
?>


<?php if ( $testimonials_query_results->have_posts() ) : ?>
		<?php while ( $testimonials_query_results->have_posts() ) :  $testimonials_query_results->the_post(); ?>
			<section class="testimonials-cont mb-sm">
				
				<div class="testimonials-body">
					<blockquote>
						<div class="lgc-testimonial"><?php the_content(); ?></div>
						<footer><?php the_title(); ?></footer>
					</blockquote>
				</div>

			</section>
		<?php endwhile; ?>
<?php endif ?>			        


<?php wp_reset_query(); ?>	