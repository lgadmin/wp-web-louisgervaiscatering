
<?php 	
	$team_members_query = array(
        'showposts' => -1,
        'post_type' => 'team_members', 
    );

    $team_members_query_results = new WP_Query( $team_members_query );
?>


<?php if ( $team_members_query_results->have_posts() ) : ?>
		<?php while ( $team_members_query_results->have_posts() ) :  $team_members_query_results->the_post(); ?>
			<section class="team-member-cont mb-sm">
				
				<div class="team-image">
					<?php the_post_thumbnail(); ?>
				</div>

				<div class="team-body">
					<h2><?php the_title(); ?> <br> <span class="h3"><?php the_field('team_job_title'); ?></span></h2>
					<?php the_content(); ?>
				</div>

			</section>
			<hr>
		<?php endwhile; ?>
<?php endif ?>			        


<?php wp_reset_query(); ?>	