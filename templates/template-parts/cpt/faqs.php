<?php 

	$myfaqterms = array(
	  'taxonomy' =>  'faq_category',
	  'orderby' =>  'meta_value_num',
	  'order' =>  'DESC',
	);

	$faq_terms = get_terms($myfaqterms); 
?>



<!-- Quick Anchors to questions -->
<h2 class="no-heading-rule">Questions about:</h2>
<ul class="faq-cats">
<?php foreach($faq_terms as $faq_term) : ?>
	<li><a href="#<?php echo $faq_term->term_id; ?>"><?php echo $faq_term->name; ?> </a></li>
<?php endforeach; ?>
</ul>




<!-- Questions Accordion Style -->
<?php foreach($faq_terms as $faq_term) : ?>
	<?php wp_reset_query(); ?>
	<?php $args = array('post_type' => 'faq',
        'tax_query' => array(
            array(
				'taxonomy' => 'faq_category',
				'field'    => 'slug',
				'terms'    => $faq_term->slug,
            ),
        ),
     ); ?>

	<?php $loop = new WP_Query($args); ?>
    
    <?php if($loop->have_posts()) : ?>
	        <h3 id="<?php echo $faq_term->term_id; ?>"><?php echo $faq_term->name; ?> </h3>

			<?php while($loop->have_posts()) : $loop->the_post() ; ?>

				<?php 
					$accordionID = 'faq-' . get_the_ID();
					$accordionID = str_replace(" ", "-", $accordionID); 
					$accordionID = strtolower($accordionID);
				?>


			    <div class="panel panel-default">
			      <div class="panel-heading" role="tab" id="headingOne">
			        <span class="panel-title">
			          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $accordionID; ?>" aria-expanded="false" aria-controls="<?php echo $accordionID; ?>">
			            <?php the_title(); ?>
			          </a>
			        </span>
			      </div>
			      <div id="<?php echo $accordionID; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			        <div class="panel-body">
			          <?php the_content(); ?>
			        </div>
			      </div>
			    </div>
	        <?php endwhile ?>
			<div class="text-right">
				<a href="#top" class="btn btn-primary">Back to Top</a>
			</div>
	<?php endif; ?>	

<?php endforeach; ?>

       
<?php wp_reset_query(); ?>	