<script>
    jQuery( document ).ready(function() {
        jQuery('.acf-slider').slick({
            infinite       : true,
            // slidesToShow   : 3,
            // slidesToScroll : 1,
            arrows: true,
            centerMode: true,
            dots: true
        });
    });
</script>

<?php  //add me in

//Fields
$images = get_sub_field('slider');

if( $images ): ?>
    <section class="<?php the_sub_field('alpha_background_colour'); ?> clearfix">
       <div class="acf-slider <?php the_sub_field('alpha_container'); ?>" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>
            <?php foreach( $images as $image ): ?>
                <div class="slick-container">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; 