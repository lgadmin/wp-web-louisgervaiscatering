<?php 
	$args = array( 
		'post_type' => 'post', 
		'posts_per_page' => 3 
	);

	$loop = new WP_Query( $args );
?>

<h2 class="h3">Latest News </h2>
<ul class="blog-feed-front">
	<?php while ( $loop->have_posts() ) : $loop->the_post()  ?>
		<li>
			<div class="img-cont">
				<?php the_post_thumbnail('medium'); ?>
			</div>
			<div class="feed-body">
				<h2><a href="<?php echo get_permalink(); ?>"> <?php the_title(); ?></a></h2>
				<span class="blog-feed-date"><?php echo get_the_date(); ?></span>
				<?php the_excerpt(); ?>
			</div>
		</li>
	<?php endwhile; ?>
</ul>

<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="btn btn-primary">More News</a>
