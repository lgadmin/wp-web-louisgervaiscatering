<?php if( have_rows('awards_slides') ): ?>
   <section class="awards-slider">
        <?php while ( have_rows('awards_slides') ) : the_row(); ?>
            <div>
                <?php echo wp_get_attachment_image( get_sub_field('awards_image'), 'full-size' ); ?>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>
