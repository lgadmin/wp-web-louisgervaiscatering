<?php if( have_rows('image_bar', 'option') ): ?>
	<div class="image_slider">
		<?php while ( have_rows('image_bar', 'option') ) : the_row();
			$image = get_sub_field('image_bar_image');
			?>
			<div class="image_bar_image">
				<h2 class="h3 image_slider_header">Awards, Etc.</h2>
				<img src="<?php echo $image['url']; ?>" alt ="<?php echo $image['alt']; ?>">
			</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>