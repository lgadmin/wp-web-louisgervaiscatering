<?php if( have_rows('featured-events-gallery') ): ?>
	<section class="featured-events-gallery-cont">
		<?php while ( have_rows('featured-events-gallery') ) : the_row(); ?>
			<a href="<?php the_sub_field('featured-events-gallery-page'); ?>" class="thumbnail">
				<div class="img-cont">
					<?php echo wp_get_attachment_image( get_sub_field('featured-events-gallery-image'), 'full-size' ); ?>
				</div>
				<div class="caption">
					<h2><?php the_sub_field('featured-events-gallery-event'); ?></h2>
					<p><?php the_sub_field('featured-events-gallery-location'); ?></p>
				</div>
			</a>
		<?php endwhile; ?>
	</section>
<?php endif; ?>