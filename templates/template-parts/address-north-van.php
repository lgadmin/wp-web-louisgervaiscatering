
<!-- Default Address Stuff -->
<div class="address-card">
	<address>
		
		<?php
			$address_alt1 = get_field('address-alt1', 'option');
			$city_alt1 = get_field('city-alt1', 'option');
			$province_alt1 = get_field('province-alt1', 'option');
			$postalcode_alt1 = get_field('postal-code-alt1', 'option');
		?>

		<p><strong><?php bloginfo('name'); ?> <?php bloginfo('description'); ?></strong></p>
		<?php $phone = get_field('company_phone', 'option'); ?>
		<span class="card-map-phone">T: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></span><br>
		<span class="card-map-phone">E: <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></span><br>
		<br>
		
		<p>
			<?php echo $address_alt1; ?>
			<br>
			<?php echo $city_alt1; ?>, <?php echo $province_alt1; ?>
			<br>
			<?php echo $postalcode_alt1; ?>
		</p>
		<br>
		<?php 
			$mapid = get_field('map_2', 'options');
			$mapshortcode = "[lg-map id=$mapid]";
		?>
		<?php echo do_shortcode( "$mapshortcode" ); ?>

		<br>
		<div><?php the_field('description-alt1', 'option'); ?></div>
		
	</address>
</div>





