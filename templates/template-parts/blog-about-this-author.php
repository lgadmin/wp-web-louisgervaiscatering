<?php 

	$author = get_the_author();
	$author_bio = get_the_author_meta('description');
	$author_image = get_avatar_url(get_the_author_meta('ID'));
?>
<h2>About author</h2>
<div class="author-container">
	<div class="avatar">
		<img src="<?php echo $author_image; ?>" alt="">
	</div>
	<div class="bio">
		<h2 class="h4 no-heading-rule"><?php echo $author; ?></h2>
		<?php echo $author_bio; ?>
	</div>
</div>
