<section class="<?php the_sub_field('alpha_background_colour'); ?> clearfix pt-sm pb-sm">
	<div class="cta-bravo <?php the_sub_field('bravo_container'); ?>">
		<div class="cta-left">
			<?php the_sub_field('bravo_left'); ?>
		</div>
		<div class="cta-right">
			<?php the_sub_field('bravo_right'); ?>
		</div>
	</div>
</section>
