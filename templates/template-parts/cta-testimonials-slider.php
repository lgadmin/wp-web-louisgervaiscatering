<?php

if (!function_exists('get_plugins')) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    
$plugins = get_plugins();
$done = false;
    
if (isset($plugins['review-stream/reviewstream.php'])) {
   	if (is_plugin_active('review-stream/reviewstream.php')) {
   		echo do_shortcode('[reviewstream]');
   		?>
   		<a href="/about/testimonials/" class="btn btn-primary">More Kind Words</a>
   		<?php 
   		$done = true;
   	}
}
if ($done != true)  {

?>
	<?php 	
		$testimonials_query = array(
	        // 'showposts'     => -1,
	        'post_type'     => 'testimonial',
	        'tax_query' => array(
	                                array(
	                            'taxonomy' => 'tl-categs', 
	                            'field' => 'slug', 
	                            'terms' => 'home-page'
	                            )
	                        )
	    );

	    $testimonials_query_results = new WP_Query( $testimonials_query );
	?>


	<?php if ( $testimonials_query_results->have_posts() ) : ?>
	<section class="testimonial-slider">
		<?php while ( $testimonials_query_results->have_posts() ) :  $testimonials_query_results->the_post(); ?>
			<div class="testimonials-cont">
				
				<div class="testimonials-body">
					<blockquote>
						<div class="lgc-testimonial"><?php the_content(); ?></div>
						<footer><?php the_title(); ?></footer>
					</blockquote>
				</div>

			</div>
		<?php endwhile; ?>
	</section>
	<?php endif ?>			        
	<a href="/about/testimonials/" class="btn btn-primary mt-sm">More Kind Words</a>

	<?php wp_reset_query(); ?>

<?php }; ?>

	