<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		
		<article id="post-<?php the_ID(); ?>" <?php post_class('type'); ?>>
			
			<div class="post-header">
				<abbr class="published" title="<?php echo get_the_date(); ?>">
					<span class="lgc-day"><?php echo get_the_date('d'); ?></span>
					<div class="yearmonthcont">
						<span class="lgc-year"><?php echo get_the_date('Y'); ?></span>
						<span class="lgc-month"><?php echo get_the_date('M'); ?></span>
					</div>
				</abbr>
			</div>
			
			<div class="post-copy">
				
				<?php the_post_thumbnail(); ?>

				<h2 class="entry-title no-heading-rule"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
				
				<p>by&nbsp;<?php the_author_link(); ?> / In <?php echo get_the_category_list(', '); ?></p>
				
				<?php the_content(); ?>
				
				<p class="small"><?php echo get_the_tag_list('Tags:&nbsp;', ', '); ?></p>
			</div>

		</article>
	<?php endwhile; ?>
<?php endif ?>

