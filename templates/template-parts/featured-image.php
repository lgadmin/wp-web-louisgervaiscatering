<?php if ( has_post_thumbnail() ) : ?>
	<div class="featured-image bg-color-overlay">
		
		<div class="title-cont">
			<div class="bg-gray-base text-white p-lg">
				<span class="featured-title"><?php echo get_the_title(); ?></span>
			</div>
		</div>

		<div class="img-cont">
			<?php the_post_thumbnail(); ?>
		</div>

	</div>
<?php endif ?>