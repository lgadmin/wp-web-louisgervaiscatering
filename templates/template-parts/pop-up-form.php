<div class="popup-form">
	<div class="cta">
		<img src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/sideoffer-bg-tab.png'; ?>" alt="">
	</div>
	<div class="form">
		<?php echo do_shortcode('[gravityform id=6 title=false description=false ajax=true tabindex=49]'); ?>
	</div>
</div>