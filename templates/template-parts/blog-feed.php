<?php 
	$args = array( 
		'post_type' => 'post', 
		'posts_per_page' => 2 
	);

	$loop = new WP_Query( $args );
?>

<ul class="blog-feed">
	<?php while ( $loop->have_posts() ) : $loop->the_post()  ?>
		<li>
			<a href="<?php echo get_permalink(); ?>"> <?php the_title(); ?></a>
			<span class="blog-feed-date"><?php echo get_the_date(); ?></span>
		</li>
	<?php endwhile; ?>
</ul>
