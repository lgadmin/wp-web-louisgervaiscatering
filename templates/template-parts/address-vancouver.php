
<!-- Default Address Stuff -->
<div class="address-card">
	<address>

		<p><strong><?php bloginfo('name'); ?> <?php bloginfo('description'); ?></strong></p>
		<?php $phone = get_field('company_phone', 'option'); ?>
		<span class="card-map-phone">T: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></span><br>
		<span class="card-map-phone">E: <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></span><br>
		<br>

		<p> 
			<?php echo do_shortcode('[lg-address1]'); ?> <br> <?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?> <br> <?php echo do_shortcode('[lg-postcode]'); ?>
		</p>

		<br>

		<?php 
			$mapid = get_field('map_1', 'options');
			$mapshortcode = "[lg-map id=$mapid]";
		?>
		<?php echo do_shortcode( "$mapshortcode" ); ?>

		<br>

		<div><?php the_field('main_address_description', 'option'); ?></div>
		
	</address>
</div>

