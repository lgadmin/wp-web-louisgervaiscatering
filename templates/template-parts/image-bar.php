<?php if( have_rows('image_bar', 'option') ): ?>
	<div class="image_bar">
		<?php while ( have_rows('image_bar', 'option') ) : the_row();
			$image = get_sub_field('image_bar_image');
			$link  = get_sub_field('image_bar_link')
			?>
			<div class="image_bar_item">
				<div class="image_bar_image">
					<a href="<?php echo $link ?>" target="_blank">
						<img src="<?php echo $image['url']; ?>" alt ="<?php echo $image['alt']; ?>">
					</a>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>