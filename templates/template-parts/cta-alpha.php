<section class="<?php the_sub_field('alpha_background_colour'); ?> clearfix pt-sm pb-sm">
	<div class="cta-alpha <?php the_sub_field('alpha_container'); ?>">
		<div class="cta-body">
			<?php the_sub_field('alpha_cw'); ?>
		</div>
	</div>
</section>
