<?php if( have_rows('quick_navs') ): ?>
	<section class="quick-nav-cont">
		<?php while ( have_rows('quick_navs') ) : the_row(); ?>
			<?php $link = get_sub_field('quick_nav_link'); ?>
			<a href="<?php echo $link['url']; ?>" class="quick-nav <?php the_sub_field('quick_nav_columns'); ?>">
				<img src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/ico-fleur.png'; ?>" alt="Louis Gervais Logo Icon">
				<h2><?php the_sub_field('quick_nav_title'); ?></h2>
				<p><?php the_sub_field('quick_nav_description'); ?></p>
			</a>
		<?php endwhile; ?>
	</section>
<?php endif; ?>