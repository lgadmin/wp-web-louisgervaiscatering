<?php if( have_rows('quick_navs_front') ): ?>
	<section class="quick-nav-front-cont">
		<?php while ( have_rows('quick_navs_front') ) : the_row(); ?>
			<div class="quick-nav-front-item">
				<div class="nav-front-content"><?php the_sub_field('quick_nav_front_content'); ?></div>
				<div class="nav-front-image"> <?php echo wp_get_attachment_image( get_sub_field('quick_nav_front_image'), 'full-size' ); ?> </div>
			</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>