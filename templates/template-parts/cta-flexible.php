<?php if( have_rows('cta') ):
	while ( have_rows('cta') ) : the_row();
		
		switch ( get_row_layout()) {

			// Alpha
			case 'cta_alpha':
				get_template_part('/templates/template-parts/cta-alpha');
			break;
			
			// Bravo
			case 'cta_bravo':
				get_template_part('/templates/template-parts/cta-bravo');
			break;

			// Slider
			case 'cta_slider':
				get_template_part('/templates/template-parts/cta-slider');
			break;

			default:
				echo "<!-- nothing to see here -->";
			break;
		}

	endwhile; else : // no layouts found 
endif; ?>