<?php if( have_rows('feature_sliders', get_option('page_for_posts')) ): ?>
   <section class="feature-slider mb-lg">
        <?php while ( have_rows('feature_sliders', get_option('page_for_posts')) ) : the_row(); ?>
            <div class="slick-container">
                <?php echo wp_get_attachment_image( get_sub_field('feature_slider_image', get_option('page_for_posts')), 'full-size' ); ?>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>
