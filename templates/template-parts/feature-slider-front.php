<?php if( have_rows('home_sliders') ): ?>
   <section class="home-slider mb-lg">
        <?php while ( have_rows('home_sliders') ) : the_row(); ?>
            <div class="slick-container">
            	<div class="img-cont">
	                <?php echo wp_get_attachment_image( get_sub_field('home_sliders_image'), 'full-size' ); ?>
            	</div>
				<a href="<?php the_sub_field('home_sliders_link'); ?>" class="title-cont">
					<?php the_sub_field('home_sliders_content'); ?>
				</a>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>
