<?php

function lg_enqueue_styles_scripts() {

    wp_enqueue_style( 'lg-style', get_stylesheet_directory_uri() . '/style.css', [], filemtime(get_template_directory() . '/style.css') );

	wp_register_script( 'lg-script', get_stylesheet_directory_uri() . "/assets/dist/js/script.min.js", array('jquery'), filemtime(get_template_directory() . "/assets/dist/js/script.min.js") );
	wp_register_script( 'vendorJS', get_stylesheet_directory_uri() . "/assets/dist/js/vendor.min.js", array('jquery'), false );
	wp_register_script( 'bootstrap', get_stylesheet_directory_uri() . "/assets/src/js/bootstrap.min.js", array('jquery'), false );


	wp_enqueue_script( 'lg-script' );
	wp_enqueue_script( 'vendorJS' );
}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


//Dequeue JavaScripts
function lg_dequeue_scripts() {
	wp_dequeue_script( '_s-navigation' );
	wp_deregister_script( '_s-navigation' );
}
//add_action( 'wp_print_scripts', 'lg_dequeue_scripts' );
 
?>