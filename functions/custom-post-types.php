<?php

//Custom Post Types
function create_post_type() {

    // Team Members
    $team_members_labels = array(
      'name'          => __( 'Team Members' ),
      'singular_name' => __( 'Team Member' ),
    );

    register_post_type( 'team_members',
        array(
          'labels'             => $team_members_labels,
          'public'             => true,
          'publicly_queryable' => false,
          'has_archive'        => false,
          'show_in_menu'       => 'louis_gervais_catering',
          'supports'           => array( 'thumbnail','title', 'editor' ),
          'taxonomies'         => array('category', 'post_tag'),
        )
    );


    // Testimonials
    $testimonials_labels = array(
      'name'          => __( 'Testimonials' ),
      'singular_name' => __( 'Testimonial' ),
    );

    register_post_type( 'testimonial',
        array(
          'labels'             => $testimonials_labels,
          'public'             => true,
          'publicly_queryable' => false,
          'has_archive'        => false,
          'show_in_menu'       => 'louis_gervais_catering',
          'supports'           => array( 'thumbnail','title', 'editor' ),
          // 'taxonomies'         => array('category', 'post_tag'),
        )
    );


    // FAQ
    $faq_labels = array(
      'name'          => __( 'FAQs' ),
      'singular_name' => __( 'FAQ' ),
    );

    register_post_type( 'faq',
        array(
          'labels'             => $faq_labels,
          'public'             => true,
          'publicly_queryable' => false,
          'has_archive'        => false,
          'show_in_menu'       => 'louis_gervais_catering',
          'supports'           => array( 'thumbnail','title', 'editor' ),
          // 'taxonomies'         => array(''category', 'post_tag''),
        )
    );

}
add_action( 'init', 'create_post_type' );

?>