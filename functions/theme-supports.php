<?php

function add_template_support() {

	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(710, 400, true); 

	add_theme_support('automatic-feed-links');

	add_theme_support( 'custom-logo', array(
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	add_theme_support( 'menus' );

	register_nav_menu( "top-nav", "Top Nav Menu(top-nav)" );
	
	register_nav_menu( "bottom-nav", "Bottom Nav Menu(bottom-nav)" );

	add_theme_support( 'html5',
	         array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
	         )
	);

	function add_svg_to_upload_mimes( $upload_mimes ) {
		$upload_mimes['svg'] = 'image/svg+xml';
		$upload_mimes['svgz'] = 'image/svg+xml';
		return $upload_mimes;
	}
	add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );

}

add_action('after_setup_theme','add_template_support', 16);

// Page Slug Body Class
function add_slug_body_class( $classes ) {
	
	global $post;
	
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// Yoast Breadcrumb support
add_theme_support( 'yoast-seo-breadcrumbs' );
add_theme_support( 'title-tag' );

add_filter( 'get_the_archive_title', function ( $title ) {

    if( is_category() ) {

        $title = single_cat_title( '', false );

    }

    return $title;

});


?>