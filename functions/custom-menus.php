<?php

function create_menu() {
    add_menu_page(
        __( 'Louis Gervais Catering', 'Louis Gervais Catering' ),
        'Louis Gervais Catering',   
        'manage_options',
        'louis_gervais_catering',
        '',
        'dashicons-admin-site',
        2
    );

    add_submenu_page(
        'louis_gervais_catering',
        __('Client Instruction'), 
        __('Client Instruction'), 
        'edit_themes', 
        'theme_cms', 
        'client_instruction'
    );

    if( function_exists('acf_add_options_page') ) {
        $option_page = acf_add_options_page(array(
            'page_title'    => 'Global Theme Options',
            'menu_title'    => 'Global Theme Options',
            'menu_slug'     => 'global-theme-options',
            'parent_slug'   => 'louis_gervais_catering',
            'capability'    => 'edit_posts',
            'redirect'  => false
        ));
    }
}

add_action( 'admin_menu', 'create_menu' );

function client_instruction(){
    require_once(get_stylesheet_directory() . '/functions/client-instruction.php');
}

?>