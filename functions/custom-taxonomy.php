<?php

function create_taxonomy(){

	// Testimonials
	register_taxonomy(
		'tl-categs',
		'testimonial',
		array(
			'label' => __( 'Testimonial' ),
			'rewrite' => array( 'slug' => 'testimonial-category' ),
			'hierarchical' => true,
		)
	);

	// FAQ
	register_taxonomy(
		'faq_category',
		'faq',
		array(
			'label' => __( 'FAQ Categories' ),
			'rewrite' => array( 'slug' => 'faq-category' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );

?>