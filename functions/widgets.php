<?php 


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function _s_widgets_init() {

	// Default
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Sidebar Blog', '_s' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);
	
	// Page Side Bar
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Sidebar Page', '_s' ),
			'id'            => 'page-side-1',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);
	
	// Contact Page
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Sidebar Contact', '_s' ),
			'id'            => 'contact-page-1',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);

	// Our Bistro Page
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Sidebar Bistro', '_s' ),
			'id'            => 'our-bistro-page-1',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);

	// Cocktail Menu
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Sidebar Cocktail', '_s' ),
			'id'            => 'cocktail-page-1',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);

	// Office Delivery
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Sidebar Office Delivery', '_s' ),
			'id'            => 'office-page-1',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);

	// Testimonials
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Sidebar Testimonials', '_s' ),
			'id'            => 'testimonials-page-1',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);

	// FAQ
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Sidebar FAQ', '_s' ),
			'id'            => 'faq-page-1',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);
}
add_action( 'widgets_init', '_s_widgets_init' );

