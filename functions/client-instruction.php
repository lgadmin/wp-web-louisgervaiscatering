<style>
	ul{
		list-style:default;
		padding-left:20px;
	}

	.border-alpha{
		border: solid 3px #f0b410;
		border-radius: 8px;
		box-shadow: 1px 1px 5px #000;
		max-width: 45vw;
	}

	.lead{
		font-size: 1.2rem;
	}
</style>

<div>
	<h1>Client Instruction</h1>

	<div style="padding: 15px; background-color:#fff; width: 1024px;">
		
		<div>
			<h2>SITE GENERAL</h2>
			<ul>
				<li><b>Logo:</b> LG Theme -> Site Settings -> Logo</li>
				<li><b>Social Media:</b> LG Theme -> Social Media</li>
				<li><b>Contact Info:</b> Dashboard-> LG Theme -> Contact</li>
				<li><b>Tracking Scripts:</b> LG Theme -> Analytics Tracking (Google tag manager setup under Longevity Account, No code injection to the header)</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Louis Gervais Custom Posts</h2>
			<ul>
				<li><b>Testimonials:</b> Louis Gervais Catering -> Testimonials</li>
				<li><b>FAQ:</b> Louis Gervais Catering -> FAQ</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Menu Icons</h2>
			<p class="lead">Including menu icons within standard wordpress editor. IE: Gluten-free, Oceanwise or Take-way friendly</p>

			<p><strong>Valid Options</strong></p>
			<ul>
				<li>Gluten-free</li>
				<li>Oceanwise</li>
				<li>Take-away friendly</li>
			</ul>

			<p>Place cursor where you wish to include an icon, insert a <i>valid option</i> and then highlight the text.</p>
			<img class="border-alpha" src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/ci-menu-icon.jpg' ?>" alt="Default TAB">
			<p>With the word still highlighted select from the <i>formats</i> drop down menu the matching valid option.</p>
			<img class="border-alpha" src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/ci-menu-icon-format.jpg' ?>" alt="Default TAB">
		</div>

		<hr>

		<div>
			<h2>PAGE STRUCTURE</h2>
			<p class="lead">Each new page works as regular Wordpress page with the exception of <strong>"Default Page"</strong> Tab. <br>This TAB controls the <strong><u>Page H1 Content, Quick Nav (Grey Boxes) and Page Slider images</u></strong>. </p>

				<h3>PAGE HEADING</h3>
				<p>This has Two inputs. <strong>Page Heading H1</strong> and <strong>Page Heading H1 small</strong>. IMPORTANT to set the H1 only in this field and not the regular wordpress editor. Setting it twice on a page could result in diminished SEO results. </p>
				<img class="border-alpha" src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/ci-default-page-tab.jpg' ?>" alt="Default TAB">

				<h3>Quick Nav</h3>
				<p>Quick nav sets the grey navigational boxes on the page. Usefull when needing a navigation on a long detailed page or linking to child pages. </p>
				<p>This is a repeating field -- click <strong>"Add Nav"</strong> to insert as many new nav items as needed.</p>
				<img class="border-alpha" src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/ci-default-page-nav.jpg' ?>" alt="Default TAB">

				<h3>Slider</h3>
				<p>Choose slider images for page.</p>
				<p>This is a repeating field -- click <strong>"Add Slider"</strong> to insert as many new sliders as needed.</p>
				<img class="border-alpha" src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/ci-default-page-slider.jpg' ?>" alt="Default TAB">
		</div>

		<div>
			<h2>Typography</h2>
			<p>The six heading elements, H1 through H6, denote section headings. Although the order and occurrence of headings is not constrained by the editor, documents should not skip levels (for example, from H1 to H3), as converting such documents to other representations is often problematic.</p>

			<p>Controling visual design of a heading without effecting the document outline order can be done by selecting H1, H2, or H3 under the format menu. IE: an h2 can be set to the same style as a h1 by selecting h1. </p>

			<img class="border-alpha" src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/ci-typography-heading-style.jpg' ?>" alt="Default TAB">

			<p>Each Heading by default has a yellow accent line. This can be turned on and off by selecting "NO RULE HEADING" from the formts menu.</p>

			<img class="border-alpha" src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/ci-typography-no-rule.jpg' ?>" alt="Default TAB">

		</div>

	</div>

</div>

