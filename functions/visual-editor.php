<?php 

// Wordpress Editor
add_editor_style();

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );


// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Gluten-free',  
			'inline' => 'span',  
			'classes' => 'tag tag-glutenfree',
			'wrapper' => false,
		), 
		array(  
			'title' => 'Oceanwise',  
			'inline' => 'span',  
			'classes' => 'tag tag-oceanwise',
			'wrapper' => false,
		), 
		array(  
			'title' => 'Take-away friendly',  
			'inline' => 'span',  
			'classes' => 'tag tag-takeaway',
			'wrapper' => false,
		), 
		array(  
			'title' => 'lead',  
			'block' => 'p',  
			'classes' => 'lead',
			'wrapper' => false,
		),  
		array(  
			'title'    => 'Button Download',  
			'selector'   => 'a',
			'classes'  => 'btn btn-download',
			'wrapper'  => false,
		), 
		array(  
			'title'    => 'Button Primary',  
			'selector'   => 'a',
			'classes'  => 'btn btn-primary',
			'wrapper'  => false,
		), 
		array(  
			'title'    => 'Button Success',  
			'selector'   => 'a',
			'classes'  => 'btn btn-success',
			'wrapper'  => false,
		), 
		array(  
			'title'    => 'Button Info',  
			'selector'   => 'a',
			'classes'  => 'btn btn-info',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'Button Warning',  
			'selector'   => 'a',
			'classes'  => 'btn btn-warning',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'Button Danger',  
			'selector'   => 'a',
			'classes'  => 'btn btn-danger',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'Button Large',  
			'selector'   => 'a',
			'classes'  => 'btn btn-lg',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'Button small',  
			'selector'   => 'a',
			'classes'  => 'btn btn-sm',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'Button Extra Small',  
			'selector'   => 'a',
			'classes'  => 'btn btn-xs',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'small',  
			'inline'   => 'span',
			'classes'  => 'small',
			'wrapper'  => true,
		),
		array(  
			'title'    => 'Text Primary',  
			'selector' => 'p,h1,h2,h3,h4,h5,h6',
			'classes'  => 'text-primary',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'Text Success',  
			'selector' => 'p,h1,h2,h3,h4,h5,h6',
			'classes'  => 'text-success',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'Text Info',  
			'selector' => 'p,h1,h2,h3,h4,h5,h6',
			'classes'  => 'text-info',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'Text Warning',  
			'selector' => 'p,h1,h2,h3,h4,h5,h6',
			'classes'  => 'text-warning',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'Text Danger',
			'selector' => 'p,h1,h2,h3,h4,h5,h6',
			'classes'  => 'text-danger',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'No Rule Heading',
			'selector' => 'h1,h2,h3,h4,h5,h6',
			'classes'  => 'no-heading-rule',
			'wrapper'  => false,
		),
		array(  
			'title'    => '2 Col List',
			'selector' => 'ul',
			'classes'  => 'ul-2col',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'h1',
			'selector' => 'h1,h2,h3,h4,h5,h6',
			'classes'  => 'h1',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'h2',
			'selector' => 'h1,h2,h3,h4,h5,h6',
			'classes'  => 'h2',
			'wrapper'  => false,
		),
		array(  
			'title'    => 'h3',
			'selector' => 'h1,h2,h3,h4,h5,h6',
			'classes'  => 'h3',
			'wrapper'  => false,
		),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  

?>