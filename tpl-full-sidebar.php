<?php
/**
 * Template Name: Full width sidebar
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container-fluid">
				<div class="body-copy">
					<?php get_template_part( 'templates/template-parts/content' ) ?>
				</div>
				<?php get_sidebar(); ?>
			</main>
		</div>
	</div>

<?php get_footer();
