<?php
/**
 * The template for displaying the footer
 *
 */
?>

	<footer id="colophon" class="bg-gray-base-alt site-footer">
		<div id="site-footer" class="clearfix">
			<div class="site-footer-alpha">
				<h2>North Vancouver</h2>
				<?php get_template_part("/templates/template-parts/address-north-van"); ?>
			</div>
			<div class="site-footer-bravo">
				<h2>Vancouver</h2>
				<?php get_template_part("/templates/template-parts/address-vancouver"); ?>
			</div>

			<div class="site-footer-charlie">
				
				<h2>Navigation</h2>
				<?php $nav_blurb = get_field('nav_blurb', 'options') ?>
				<p><?php echo $nav_blurb ?></p>
				<?php get_template_part("/templates/template-parts/nav-footer"); ?>
				<?php echo do_shortcode("[instagram-feed showbutton=false showfollow=false showheader=false]"); ?>
			</div>

			<div class="site-footer-delta">
				<h2>Latest Posts</h2>
				<?php get_template_part("/templates/template-parts/blog-feed"); ?>
				<h2>Stay Connected</h2>
				<?php echo do_shortcode( '[lg-social-media]' ); ?>
				<?php echo do_shortcode('[fts_facebook id=LouisGervaisFineFoodsCatering posts=1 posts_displayed=page_only type=page]'); ?>
			</div>
		</div>
	</footer><!-- #colophon -->
		
	<div id="site-legal" class="bg-gray-base clearfix">
		<div class="site-info"><?php get_template_part("/templates/template-parts/site-info"); ?></div>
		<div class="site-longevity"> <?php get_template_part("/templates/template-parts/site-footer-longevity"); ?> </div>
	</div>

	<?php get_template_part("/templates/template-parts/pop-up-form"); ?>


<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1065660978;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1065660978/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php wp_footer(); ?>
<!-- <script src="https://scripts.mymarketingreports.com/js.php?nt_id=10013481"></script> -->
</body>
</html>
